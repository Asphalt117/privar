﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using WebUI.Models;

namespace WebUI.Helpers
{
    public static class Pagination
    {
        public static MvcHtmlString PageNavigatorInfo(this HtmlHelper helper, PageInfo pageInfo)
        { 
            StringBuilder navString = new StringBuilder();
            int curPageNum = pageInfo.pageNum + 1;
            int pagesCount = (pageInfo.itemsCount / pageInfo.pageSize) + (pageInfo.itemsCount % pageInfo.pageSize > 0 ? 1 : 0); // Общее число страниц
            navString.Append("<div class=\"text-center page-info\"><span>Страница " + (pagesCount < curPageNum ? 0 : curPageNum).ToString() + " из " + pagesCount.ToString() + "</span></div>");
            return MvcHtmlString.Create(navString.ToString());
        }

        // Karasiov aka TEREK, BootStrap page navigator...............................................................................................................................
        public static MvcHtmlString PageNavigatorBootStrap(this HtmlHelper helper, PageInfo pageInfo, string actionName, Func<int, object> paramList)
        {
            StringBuilder navString = new StringBuilder();
            TagBuilder emptyLink = new TagBuilder("a");
            emptyLink.MergeAttribute("href", "#");
            emptyLink.MergeAttribute("role", "button");
         
            int curPageNum = pageInfo.pageNum + 1;
            int pagesCount = (pageInfo.itemsCount / pageInfo.pageSize) + (pageInfo.itemsCount % pageInfo.pageSize > 0 ? 1 : 0); // Общее число страниц
            int midPageNum = (pagesCount / 2) + (pagesCount % 2 > 0 ? 1 : 0);
            int leftCount = 0;
            int rightCount = 0;

            if (curPageNum <= midPageNum)
            {
                leftCount = ((pageInfo.vsblPagesCount - 1) / 2);
                rightCount = ((pageInfo.vsblPagesCount - 1) / 2) + ((pageInfo.vsblPagesCount - 1) % 2 > 0 ? 1 : 0);
            }
            else
            {
                leftCount = ((pageInfo.vsblPagesCount - 1) / 2) + ((pageInfo.vsblPagesCount - 1) % 2 > 0 ? 1 : 0);
                rightCount = ((pageInfo.vsblPagesCount - 1) / 2);
            }

            int startPageNum = curPageNum - leftCount;
            int endPageNum = curPageNum + rightCount;
            if (startPageNum <= 0)
            {
                startPageNum = 1;
                endPageNum = pageInfo.vsblPagesCount > pagesCount ? pagesCount : pageInfo.vsblPagesCount;
            }

            if (endPageNum > pagesCount)
            {
                endPageNum = pagesCount;
                startPageNum = pagesCount - pageInfo.vsblPagesCount <= 0 ? 1 : pagesCount - pageInfo.vsblPagesCount;
            }

            navString.Append("<div class=\"col-md-12 text-center container-fluid clearfix page-navigator\"><ul class=\"pagination\">");

            TagBuilder strToFirst = new TagBuilder("li");   // to first
            if (curPageNum == 1)
            {
                strToFirst.AddCssClass("disabled");
                emptyLink.MergeAttribute("rel", "first");
                emptyLink.MergeAttribute("aria_label", "First");
                emptyLink.InnerHtml = "&laquo;";
                strToFirst.InnerHtml = emptyLink.ToString();
            }
            else { strToFirst.InnerHtml = helper.ActionLink(HttpUtility.HtmlDecode("&laquo;"), actionName, paramList(0), new { @aria_label = "First", @rel = "first", @role = "button" }).ToHtmlString(); }
            navString.Append(strToFirst.ToString());

            TagBuilder strToPrev = new TagBuilder("li");
            if (curPageNum == 1) 
            {
                strToPrev.AddCssClass("disabled");
                emptyLink.MergeAttribute("rel", "previous", true);
                emptyLink.MergeAttribute("aria_label", "Previous", true);
                emptyLink.InnerHtml = "&lsaquo;";
                strToPrev.InnerHtml = emptyLink.ToString();
            }
            else { strToPrev.InnerHtml = helper.ActionLink(HttpUtility.HtmlDecode("&lsaquo;"), actionName, paramList(curPageNum - 2), new { @aria_label = "Previous", @rel = "previous", @role = "button" }).ToHtmlString(); }
            navString.Append(strToPrev.ToString());

            for (int i = 0; i < (endPageNum - startPageNum + 1); i++)
            {
                TagBuilder strToCentral = new TagBuilder("li");
                if (startPageNum + i == curPageNum) { strToCentral.AddCssClass("active"); }
                strToCentral.InnerHtml = helper.ActionLink((startPageNum + i).ToString(), actionName, paramList((startPageNum + i) - 1), new { @role = "button" }).ToHtmlString();
                navString.Append(strToCentral.ToString());
            }

            TagBuilder strToNext = new TagBuilder("li");   // to next
            if (curPageNum == pagesCount)
            {
                strToNext.AddCssClass("disabled");
                emptyLink.MergeAttribute("rel", "next", true);
                emptyLink.MergeAttribute("aria_label", "Next", true);
                emptyLink.InnerHtml = "&rsaquo;";
                strToNext.InnerHtml = emptyLink.ToString();
            }
            else { strToNext.InnerHtml = helper.ActionLink(HttpUtility.HtmlDecode("&rsaquo;"), actionName, paramList(curPageNum), new { @aria_label = "Next", @rel = "next", @role = "button" }).ToHtmlString(); }
            navString.Append(strToNext.ToString());

            TagBuilder strToLast = new TagBuilder("li");    // to last
            if (curPageNum == pagesCount)
            {
                strToLast.AddCssClass("disabled");
                emptyLink.MergeAttribute("rel", "last", true);
                emptyLink.MergeAttribute("aria_label", "Last", true);
                emptyLink.InnerHtml = "&raquo;";
                strToLast.InnerHtml = emptyLink.ToString();
            }
            else { strToLast.InnerHtml = helper.ActionLink(HttpUtility.HtmlDecode("&raquo;"), actionName, paramList(pagesCount - 1), new { @aria_label = "Last", @rel = "last", @role = "button" }).ToHtmlString(); }
            navString.Append(strToLast.ToString());
            navString.Append("</ul></div>");
            return MvcHtmlString.Create(navString.ToString());
        }
    }
}