﻿using System;
using System.Web.Mvc;
using Domain.Engine;
using Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class SreestrController : Controller
    {
        private AbzContext db = new AbzContext();
        [Authorize]
        public ActionResult Sale()
        {
            Mydate mydate = new Mydate();
            mydate.dat = DateTime.Today;
            mydate.Cdat = DateToString.CDat(mydate.dat);

            return View(mydate);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Sale(Mydate cdt)
        {
            return View(cdt);
        }

        public ActionResult SelDat(Mydate cdt)
        {
            if (cdt.Cdat == null)
            {
                cdt = new Mydate();
                cdt.dat = DateTime.Today;
                cdt.Cdat = DateToString.CDat(cdt.dat);

            }
            DateTime dd = StringToDate.Date(cdt.Cdat);
            List<SaleReestr> reestr = db.SaleReestrs.Where(s => s.Ddat == dd).ToList();
            decimal smr = reestr.Sum(n => n.Kol);
            string csm = smr.ToString();
            ViewBag.smr = "Итого за " + cdt.Cdat + " - " + csm;
            return PartialView(reestr);
        }
        [Authorize]
        public ActionResult Sup()
        {
            Mydate mydate = new Mydate();
            mydate.dat = DateTime.Today;
            mydate.Cdat = DateToString.CDat(mydate.dat);

            return View(mydate);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Sup(Mydate cdt)
        {
            return View(cdt);
        }
        public ActionResult SupDat(Mydate cdt)
        {
            if (cdt.Cdat == null)
            {
                cdt = new Mydate();
                cdt.dat = DateTime.Today;
                cdt.Cdat = DateToString.CDat(cdt.dat);

            }
            DateTime dd = StringToDate.Date(cdt.Cdat);
            List<SupReestr> reestr = db.SupReestrs.Where(s => s.Ddat == dd).ToList();                
            return PartialView(reestr);
        }

        public ActionResult MainMenu(string menuItem)
        {
            ViewBag.MenuItem = menuItem;
            return PartialView();
        }

    }
}