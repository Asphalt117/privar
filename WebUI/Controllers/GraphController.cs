﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Engine;
using Domain.Entities;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class GraphController : Controller
    {
        private AbzContext db = new AbzContext();

        [Authorize]
        public ActionResult Sale()
        {
            return View(db.SaleGraphs.ToList());
        }
        [Authorize]
        public ActionResult Sup()
        {
            return View(db.SupGraphs.ToList());
        }
        [Authorize]
        public ActionResult PreSale()
        {
            return View(db.SalePreGraphs.ToList());
        }
        [Authorize]
        public ActionResult PreSaleDat()
        {
            Mydate mydate = new Mydate();
            mydate.dat = DateTime.Today;
            mydate.Cdat = DateToString.CDat(mydate.dat);
            return View(mydate);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PreSaleDat(Mydate cdt)
        {
            return View(cdt);
        }

        public ActionResult SelDat(Mydate cdt)
        {
            if (cdt.Cdat == null)
            {
                cdt = new Mydate();
                cdt.dat = DateTime.Today;
                cdt.Cdat = DateToString.CDat(cdt.dat);

            }
            DateTime dd = StringToDate.Date(cdt.Cdat);

            List<SalePreGraph> reestr = db.SalePreGraphs.Where(s => s.Ddat == dd).ToList();

            //decimal smr = reestr.Sum(n => n.Kol);
            //string csm = smr.ToString();
            //ViewBag.smr = "Итого за " + cdt.Cdat + " - " + csm;
            return PartialView(reestr);
        }

        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}