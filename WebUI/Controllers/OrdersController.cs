﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Domain.Entities;
using Domain.ModelView;
using Domain.Repository;

namespace WebUI.Controllers
{
    public class OrdersController : BaseController
    {
        OrderRepository repo = new OrderRepository();
        [Authorize]
        public ActionResult Index()
        {
            ViewBag.MenuItem = "orders";
            List<OrderView> order = repo.GetOrders(Cust.ID_CUSTOM);
            return View(order);
        }

        public async Task<ActionResult> Create()
        {
            OrderV order = await repo.GetCreate(Cust.ID_CUSTOM);
            SetCookie("OrderId", order.Order.OrderId);
            return RedirectToAction("GoodSelect");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OrderV order, string Gd)
        {
            if (ModelState.IsValid)
            {
                if (Gd != null)
                {
                    return RedirectToAction("GoodSelect");
                }

                return RedirectToAction("Index");
            }

            return View(order);
        }

        public ActionResult DelProducts(int id)
        {
            OrderProductView good = db.OrderProductViews.Find(id);
            return View(good);
        }

        [HttpPost]
        public ActionResult DelProducts(OrderProductView ord,int id)
        {
            OrderProduct prod = db.OrderProducts.Find(id);
            db.OrderProducts.Remove(prod);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Copy(int id)
        {
            OrderV ord = repo.GetCopy(id);
            SetCookie("OrderId", ord.Order.OrderId);
            return View("Details", ord);
        }

        public ActionResult GoodSelect()
        {
            int ord = GetIntCookie("OrderId");
            List<GoodSelect> goods = repo.GetGood(ord);
            return View(goods);
        }

        public ActionResult Details(int id,decimal qty)
        {
            int ord = GetIntCookie("OrderId");
            OrderProduct prod = new OrderProduct();
            prod.Quant = qty;
            prod.ProductId = id;
            prod.OrderId = ord;
            db.OrderProducts.Add(prod);
            db.SaveChanges();
            OrderV order = repo.GetOrderV(ord);
            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Booking(OrderV ord, string Gd, int? ProductId, decimal qty, string Recalc)
        {
            if (Gd != null)
                return RedirectToAction("GoodSelect");
            if (Recalc != null)
            {
                OrderProduct good = db.OrderProducts.Find(ProductId);
                good.Quant = qty;
                db.Entry(good).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit");
            }
            ord.Order.CustId = Cust.ID_CUSTOM;
            repo.Save(ord);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? id)
        {
            OrderView order;
            if (id == null)
            {
                int orderid = GetIntCookie("OrderId");
                order = db.OrderViews.Find(orderid);
            }
            else
            {
                order = db.OrderViews.Find(id);    
            }
            
            if (order == null)
            {
                return HttpNotFound();
            }
            OrderV ord = new OrderV();
            ord.Order = order;
            ord.Prod = db.OrderProductViews.Where(p => p.OrderId == ord.Order.OrderId).ToList();
            return View("Details", ord);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrderId,CustId,AdresId,Centr,Dat,note,insDate,StatusId")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View("Details",order);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderView order = db.OrderViews.Find(id);
            OrderV ord = new OrderV();
            ord.Order = order;
            ord.Prod = db.OrderProductViews.Where(p => p.OrderId == ord.Order.OrderId).ToList();
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(ord);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
