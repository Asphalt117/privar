﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using Domain.Repository;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
        public Cust Cust;
        public AbzContext db = new AbzContext();
        [Authorize]
        public ActionResult Index()
        {
            CustRepository repo = new CustRepository();
            //string ip = HttpContext.Request.UserHostAddress;
            //int qq=repo.SaveUser(User.Identity.Name, 0, ip);
            return RedirectToAction("Sale", "Graph");
            
            ViewBag.MenuItem = "profile";
            if (User.IsInRole("AdminPS"))
            {
                SetCookie("CustId", "");
                SetCookie("UserId", "");
                return RedirectToAction("Index", "Admin");
            }
            if (User.IsInRole("ABZ"))
            {
                string ip = HttpContext.Request.UserHostAddress;
                repo.SaveUser(User.Identity.Name, 0, ip);
                return RedirectToAction("Sale", "Graph");
            }
            UserInCust cust = db.UserInCusts.FirstOrDefault(a => a.UserId == User.Identity.Name);
            if (cust != null)
            {
                string CustId = cust.CustID.ToString();
                SetCookie("CustId", CustId);
                SetCookie("UserId", User.Identity.Name);
                string ip = HttpContext.Request.UserHostAddress;
                Cust = repo.CustUser(User.Identity.Name, cust.CustID,ip);
                return View(Cust);
            }
            return View("NoCust");
        }

        public void SetCookie(string cookieName, string cookieValue)
        {
            Response.Cookies[cookieName].Value = cookieValue;
            Response.Cookies[cookieName].Expires = DateTime.Now.AddDays(1);
        }

        //public ActionResult MainMenu(string menuItem)
        //{
        //    ViewBag.MenuItem = menuItem;
        //    return PartialView();
        //}

        public ActionResult FooterMenu()
        {
            return PartialView();
        }
    }
}