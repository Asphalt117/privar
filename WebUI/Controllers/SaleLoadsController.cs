﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;

namespace WebUI.Controllers
{
    public class SaleLoadsController : Controller
    {
        private AbzContext db = new AbzContext();

        // GET: SaleLoads
        [Authorize]
        public ActionResult Index()
        {
            return View(db.SaleLoads.Where(a=>a.Ismark==2).ToList());
        }

        //// GET: SaleLoads/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    SaleLoad saleLoad = db.SaleLoads.Find(id);
        //    if (saleLoad == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(saleLoad);
        //}

        //// GET: SaleLoads/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: SaleLoads/Create
        //// Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        //// сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ID,ID_CUSTOM,Cust,Driv,Model,Ab,Gn,Good,centr,Dat")] SaleLoad saleLoad)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.SaleLoads.Add(saleLoad);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(saleLoad);
        //}

        //// GET: SaleLoads/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    SaleLoad saleLoad = db.SaleLoads.Find(id);
        //    if (saleLoad == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(saleLoad);
        //}

        //// POST: SaleLoads/Edit/5
        //// Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        //// сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ID,ID_CUSTOM,Cust,Driv,Model,Ab,Gn,Good,centr,Dat")] SaleLoad saleLoad)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(saleLoad).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(saleLoad);
        //}

        //// GET: SaleLoads/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    SaleLoad saleLoad = db.SaleLoads.Find(id);
        //    if (saleLoad == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(saleLoad);
        //}

        //// POST: SaleLoads/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    SaleLoad saleLoad = db.SaleLoads.Find(id);
        //    db.SaleLoads.Remove(saleLoad);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
