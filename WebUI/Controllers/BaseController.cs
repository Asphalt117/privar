﻿using Domain.Entities;
using System.Web.Mvc;
using System.Web;
using System;
using Domain.Repository;

namespace WebUI.Controllers
{
    public class BaseController : Controller
    {
        public Cust Cust;
        public AbzContext db = new AbzContext();
        //string UserId;
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            Cust = GetCookieCust();
            CustRepository repo = new CustRepository();
            string UserId = GetCookie("UserId");
            if (!String.IsNullOrWhiteSpace(UserId) && Cust != null)
            {
                string ip = HttpContext.Request.UserHostAddress;
                repo.CustUser(UserId, Cust.ID_CUSTOM,ip);
            }
                
        }
        public Cust GetCookieCust()
        {
            int CustID = 0;
            string custId = GetCookie("CustId");
            if (!String.IsNullOrWhiteSpace(custId))
                CustID = Convert.ToInt32(custId);
            Cust cust = db.Custs.Find(CustID);
            return cust;
        }
        public string GetCookie(string cookieName)
        {
            string cookieValue = "";
            HttpCookie cookie = Request.Cookies[cookieName];
            if (cookie != null)
            {
                cookieValue = cookie.Value;
            }
            return cookieValue;
        }

        public int GetIntCookie(string cookieName)
        {
            int cookieValue = 0;
            HttpCookie cookie = Request.Cookies[cookieName];
            if (cookie != null)
            {
                cookieValue = Convert.ToInt32(cookie.Value);
            }
            return cookieValue;
        }

        public void SetCookie(string cookieName, int cookieValue)
        {
            string StrValue = cookieValue.ToString();
            Response.Cookies[cookieName].Value = StrValue;
            Response.Cookies[cookieName].Expires = DateTime.Now.AddDays(1);
        }

    }
}