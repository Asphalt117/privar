﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Domain.Entities;

namespace WebUI.Controllers
{
    public class SaleController : BaseController
    {
        private AbzContext db = new AbzContext();
        [Authorize]
        public ActionResult Index()
        {
            List<SaleGraph> slgr = db.SaleGraphs.Where(c => c.Id_custom == Cust.ID_CUSTOM).ToList();
            return View(slgr);
        }

        public ActionResult Details(int id)
        {
            List<Ttn> ttns = db.Ttns.Where(w => w.Id_GrafSal == id)
                    .OrderBy(b => b.Num).ToList();

            return View(ttns);
        }
    
    }
}