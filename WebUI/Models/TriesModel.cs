﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebUI.Models
{
    public class TriesModel
    {
        public short CountTries { get; set; }
        public string ReturnUrl { get; set; }
    }
}