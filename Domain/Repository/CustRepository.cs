﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using System.Data.Entity;

namespace Domain.Repository
{
    public class CustRepository
    {
        private readonly AbzContext db = new AbzContext();
        public Cust CustUser(string userId, int custId,string ip)
        {
            Statistic statistic = new Statistic();
            statistic.CustID = custId;
            statistic.UserId = userId;
            statistic.UserAdres = ip;
            db.Statistics.Add(statistic);
            db.SaveChanges();

            UserInCust userInCust = db.UserInCusts.FirstOrDefault(u => u.UserId == userId && u.CustID == custId);
            if (userInCust != null)
            {
                userInCust.LastDat = DateTime.Now;
                db.Entry(userInCust).State = EntityState.Modified;
                db.SaveChanges();
            }

            return db.Custs.Find(custId);
        }

        public int SaveUser(string userId, int custId, string ip)
        {
            Statistic statistic = new Statistic();
            statistic.CustID = custId;
            statistic.UserId = userId;
            statistic.UserAdres = ip;
            db.Statistics.Add(statistic);
            db.SaveChanges();
            return 1;
        }

    }
}
