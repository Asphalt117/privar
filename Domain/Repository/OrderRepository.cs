﻿using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using System.Web.Mvc;
using System;
using System.Data.Entity;
using Domain.Engine;
using System.Threading.Tasks;
using System.Data;
using Domain.ModelView;
using EntityState = System.Data.Entity.EntityState;


namespace Domain.Repository
{
    public class OrderRepository
    {
        private AbzContext db = new AbzContext();
        private OrderV sv = new OrderV();

        public void Save(OrderV ord)
        {
            Order order=new Order();
            order.Centr = ord.Order.Centr;
            order.Dat = StringToDate.Date(ord.Order.cDat);
            order.StatusId = 1;
            order.note = ord.Order.Note;
            order.OrderId = ord.Order.OrderId;
            order.AdresId = ord.Order.AdresId;
            order.CustId = ord.Order.CustId;
            db.Entry(order).State = EntityState.Modified;
            db.SaveChanges();
        }

        public List<GoodSelect> GetGood(int orderId)
        {
            List<GoodSelect> goods=new List<GoodSelect>();

            List<Good> gds = db.Goods.ToList();
            List<OrderProduct> prod = db.OrderProducts.Where(p => p.OrderId == orderId).ToList();
            foreach (var g in gds)
            {
                GoodSelect good = new GoodSelect();
                good.ID = g.ID;
                good.txt = g.txt;
                good.Unit = g.Unit;
                OrderProduct pr = prod.FirstOrDefault(p => p.ProductId == g.ID);
                if (pr!=null)
                    good.Quant = pr.Quant;

                goods.Add(good);
            }
            return goods;
        }

        public OrderView GetOrder(int id)
        {
            OrderView order= db.OrderViews.Find(id);
            return order;                
        }

        public OrderV GetOrderV(int id)
        {
            OrderV order = new OrderV();
            order.Order = db.OrderViews.Find(id);
            order.Prod = db.OrderProductViews.Where(p => p.OrderId == id).ToList();
            return order;
        }

        public List<OrderView> GetOrders(int id)
        {
            return db.OrderViews.Where(o => o.CustId == id && o.StatusId > 0).ToList();
        }

        public async Task<OrderV> GetCreate(int custid)
        {
            Order order = new Order();
            order.CustId = custid;
            order.AdresId = 1;
            order.Dat = DateTime.Now.AddDays(1);
            order.StatusId = 0;
            db.Orders.Add(order);
            int id = await db.SaveChangesAsync();

            List<OrderProductView> products = new List<OrderProductView>();
            OrderV orderv = new OrderV();
            orderv.Order = await db.OrderViews.FindAsync(order.OrderId); 
            orderv.Prod = products;
            return orderv;
        }

        public async Task<OrderProductView> SaveProd(int goodid,int orderid)
        {
            OrderProduct product = new OrderProduct();
            product.ProductId = goodid;
            product.OrderId = orderid;
            db.OrderProducts.Add(product);
            int id = await db.SaveChangesAsync();
            OrderProductView order = await db.OrderProductViews.FindAsync(product.OrderProductId);
            return order;
        }

        public OrderV GetCopy(int id)
        {
            OrderV orderV = GetOrderV(id);
            int custid = orderV.Order.CustId;
            Order order=new Order();
            order.CustId = custid;
            order.AdresId = 1;
            order.Dat = DateTime.Now.AddDays(1);
            order.StatusId = 0;
            db.Orders.Add(order);
            int idnew = db.SaveChanges();
            //OrderProductView orderProduct;
            //int idprod; 
            foreach (var prod in orderV.Prod)
            {
                OrderProduct product = new OrderProduct();
                product.ProductId = prod.ProductId;
                product.OrderId = order.OrderId;
                product.Quant = prod.Quant;
                db.OrderProducts.Add(product);
                db.SaveChanges();
            }

            return GetOrderV(order.OrderId);
        }
    }
}
