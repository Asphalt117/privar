﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Domain.Entities
{
    [Table("Adres")]
    public class Adres
    {
        [Key]
        public int id_Adres { get; set; }
        [Display(Name = "Адрес")]
        public string txt { get; set; }
        [Display(Name = "Расстояние")]
        public int Rast { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? ID_CUSTOM { get; set; }

    }
}