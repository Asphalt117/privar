﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using System;

namespace Domain.Entities
{
    [Table("bGood")]
    public class Good
    {
        [Key]
        public int ID { get; set; }
        [Display(Name = "Продукция")]
        public string txt { get; set; }
        public string Unit { get; set; }

    }
}
