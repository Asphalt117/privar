﻿using System.Data.Entity;

namespace Domain.Entities
{
    public class AbzContext : DbContext
    {
        public AbzContext()
            : base("name=AbzConnection")
                {
                }
        public DbSet<Cust> Custs { get; set; }
        public DbSet<Statistic> Statistics { get; set; }
        public DbSet<UserInCust> UserInCusts { get; set; }
        public DbSet<UserAdmin> UserAdmins { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderProduct> OrderProducts { get; set; }
        public DbSet<OrderView> OrderViews { get; set; }
        public DbSet<Adres> Adreses { get; set; }
        public DbSet<Good> Goods { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<OrderProductView> OrderProductViews { get; set; }
        public DbSet<Ttn> Ttns { get; set; }
        public DbSet<SupGraph> SupGraphs { get; set; }
        public DbSet<SalePreGraph> SalePreGraphs { get; set; }
        public DbSet<SaleGraph> SaleGraphs { get; set; }
        public DbSet<SaleReestr> SaleReestrs { get; set; }
        public DbSet<SupReestr> SupReestrs { get; set; }
        public DbSet<SaleLoad> SaleLoads { get; set; }
    }
}
