﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    [Table("bSup")]
    public class SupReestr
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "№ ТТН")]
        public string Num { get; set; }
        [Display(Name = "Материал")]
        public string Good { get; set; }
        [Display(Name = "Контрагент")]
        public string Cust { get; set; }

        [DisplayName("Дата-время")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime DatIn { get; set; }

        [Display(Name = "Вес")]
        public decimal Qsend { get; set; }

        [Display(Name = "Водитель")]
        public string Driv { get; set; }
        [Display(Name = "Гос.№")]
        public string Gn { get; set; }
        [Display(Name = "Модель")]
        public string Model { get; set; }
        public DateTime Ddat { get; set; }
    }
}
