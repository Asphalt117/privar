﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Domain.Entities
{
    [Table("bSaleLstCar")]
    public class SaleLoad
    {
        [Key]
        public int   ID { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int    ID_CUSTOM { get; set; }
        public string Cust { get; set; }
        public string Driv { get; set; }
        public string Model { get; set; }
        public string Ab { get; set; }
        public string Gn { get; set; }
        public string Good { get; set; }
        public Int16    Centr { get; set; }
        public DateTime Dat { get; set; }
        public Int16 Ismark { get; set; }
    }
}
