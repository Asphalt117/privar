﻿using System;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace Domain.Entities
{
    [Table("bOrder")]
    public class OrderView
    {
        [Key]
        public int OrderId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int CustId { get; set; }
        public int AdresId { get; set; }
        //public int GoodId { get; set; }

        [DisplayName("Продукция")]
        public string Good { get; set; }

        [DisplayName("Адрес")]
        public string Adres { get; set; }

        public bool Centr { get; set; }
        [Column(TypeName = "date")]
        public DateTime Dat { get; set; }
        public string cDat { get; set; }
        public string Note { get; set; }
        public string Status { get; set; }
        public int StatusId { get; set; }
        [Display(Name = "Количество")]
        public decimal? Qty { get; set; }        

        //public SelectList SelectAdres { get; set; }
        //public List<OrderProductView> Products { get; set; }
    }
}
