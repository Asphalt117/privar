﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
     [Table("bGraphSale")]
    public class SaleGraph
    {
        [Key]
        public int Id { get; set; }
        public int Id_custom { get; set; }

        [Display(Name = "Материал")]
        public string Good { get; set; }
        [Display(Name = "Контрагент")]
        public string Cust { get; set; }
        [Display(Name = "Отпущено")]
        public decimal? Kol { get; set; }
        [Display(Name = "Адрес")]
        public string Adres { get; set; }
        [Display(Name = "Заказано")]
        public decimal Ord { get; set; }

    }
}
