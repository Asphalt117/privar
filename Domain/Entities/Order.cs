namespace Domain.Entities
{
    using System;
    using System.Web.Mvc;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Orders")]
    public class Order
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        public int OrderId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int CustId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? AdresId { get; set; }
        public bool Centr { get; set; }
        [Column(TypeName = "date")]
        public DateTime Dat { get; set; }
        public string note { get; set; }
        public DateTime? insDate { get; set; }
        public int StatusId { get; set; }
    }
}
