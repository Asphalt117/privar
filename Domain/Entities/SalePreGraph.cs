﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    [Table("bPreGraphSale")]
    public class SalePreGraph
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Материал")]
        public string Good { get; set; }
        [Display(Name = "Контрагент")]
        public string Cust { get; set; }
        [Display(Name = "Заказано")]
        public decimal Kol { get; set; }
        [Display(Name = "Адрес")]
        public string Adres { get; set; }
        [Display(Name = "Примечание")]
        public string Notes { get; set; }

        //[Column(TypeName = "date")]
        //[Display(Name = "Дата-время")]
        //public DateTime Dat { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm}", ApplyFormatInEditMode = true)]
        [Display(Name = "Дата-время")]
        public DateTime Dat { get; set; }

        public DateTime Ddat { get; set; }
    }
}
