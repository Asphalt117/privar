﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    [Table("bSupGr")]
    public class SupGraph
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Материал")]
        public string Good { get; set; }

        [Display(Name = "Поставщик")]
        public string Sender { get; set; }
        [Display(Name = "Поставлено")]
        public decimal Qty { get; set; }
        [Display(Name = "Кол-во")]
        public decimal Kol { get; set; }
    }
}
