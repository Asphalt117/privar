﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    [Table("Custom")]
    public class Cust
    {
        [Key]
        public int ID_CUSTOM { get; set; }
        [Required]
        [DisplayName("Полное наименование")]
        public string txtfull { get; set; }
        [DisplayName("ИНН")]
        public string Inn { get; set; }

        [DisplayName("Адрес")]
        public string Adres { get; set; }
        [DisplayName("Краткое наименование")]
        public string txt { get; set; }

        //[DisplayName("Код 1С")]
        //public string Cod1s { get; set; }
    }
}
