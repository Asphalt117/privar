﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ModelView
{
    public class GoodSelect
    {
        [Key]
        public int ID { get; set; }
        [Display(Name = "Продукция")]
        public string txt { get; set; }
        public string Unit { get; set; }
        [Column(TypeName = "numeric")]
        public decimal? Quant { get; set; }
    }
}
