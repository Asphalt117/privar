﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using  Domain.Entities;
namespace Domain.ModelView
{
    public class OrderV
    {
        public OrderView Order { get; set; }
        public List<OrderProductView> Prod { get; set; }
    }
}
